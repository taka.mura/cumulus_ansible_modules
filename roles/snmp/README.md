
# Cumulus SNMP Role

Configure Simple Network Management Protocol

Variable | Choices/Defaults | Type
--- | --- | ---
snmp.addresses|list of snmp servers.|List of Strings
snmp.rocommunity|read-only community string string.|String

## Examples
<details><summary markdown="span">evpn_centralized</summary>
border01
<pre><code>snmp:
  addresses:
  - 192.168.200.63/24@mgmt
  - udp6:[::1]:161
  rocommunity: public
</code></pre>
border02
<pre><code>snmp:
  addresses:
  - 192.168.200.64/24@mgmt
  - udp6:[::1]:161
  rocommunity: public
</code></pre>
fw1
<pre><code>snmp:
  addresses:
  - False/24@mgmt
  - udp6:[::1]:161
  rocommunity: public
</code></pre>
leaf01
<pre><code>snmp:
  addresses:
  - 192.168.200.11/24@mgmt
  - udp6:[::1]:161
  rocommunity: public
</code></pre>
leaf02
<pre><code>snmp:
  addresses:
  - 192.168.200.12/24@mgmt
  - udp6:[::1]:161
  rocommunity: public
</code></pre>
leaf03
<pre><code>snmp:
  addresses:
  - 192.168.200.13/24@mgmt
  - udp6:[::1]:161
  rocommunity: public
</code></pre>
leaf04
<pre><code>snmp:
  addresses:
  - 192.168.200.14/24@mgmt
  - udp6:[::1]:161
  rocommunity: public
</code></pre>
server01
<pre><code>snmp:
  addresses:
  - False/24@mgmt
  - udp6:[::1]:161
  rocommunity: public
</code></pre>
server02
<pre><code>snmp:
  addresses:
  - False/24@mgmt
  - udp6:[::1]:161
  rocommunity: public
</code></pre>
server04
<pre><code>snmp:
  addresses:
  - False/24@mgmt
  - udp6:[::1]:161
  rocommunity: public
</code></pre>
server05
<pre><code>snmp:
  addresses:
  - False/24@mgmt
  - udp6:[::1]:161
  rocommunity: public
</code></pre>
spine01
<pre><code>snmp:
  addresses:
  - 192.168.200.21/24@mgmt
  - udp6:[::1]:161
  rocommunity: public
</code></pre>
spine02
<pre><code>snmp:
  addresses:
  - 192.168.200.22/24@mgmt
  - udp6:[::1]:161
  rocommunity: public
</code></pre>
spine03
<pre><code>snmp:
  addresses:
  - 192.168.200.23/24@mgmt
  - udp6:[::1]:161
  rocommunity: public
</code></pre>
spine04
<pre><code>snmp:
  addresses:
  - 192.168.200.24/24@mgmt
  - udp6:[::1]:161
  rocommunity: public
</code></pre>
</details>
<details><summary markdown="span">evpn_l2only</summary>
border01
<pre><code>snmp:
  addresses:
  - 192.168.200.63/24@mgmt
  - udp6:[::1]:161
  rocommunity: public
</code></pre>
border02
<pre><code>snmp:
  addresses:
  - 192.168.200.64/24@mgmt
  - udp6:[::1]:161
  rocommunity: public
</code></pre>
fw1
<pre><code>snmp:
  addresses:
  - False/24@mgmt
  - udp6:[::1]:161
  rocommunity: public
</code></pre>
leaf01
<pre><code>snmp:
  addresses:
  - 192.168.200.11/24@mgmt
  - udp6:[::1]:161
  rocommunity: public
</code></pre>
leaf02
<pre><code>snmp:
  addresses:
  - 192.168.200.12/24@mgmt
  - udp6:[::1]:161
  rocommunity: public
</code></pre>
leaf03
<pre><code>snmp:
  addresses:
  - 192.168.200.13/24@mgmt
  - udp6:[::1]:161
  rocommunity: public
</code></pre>
leaf04
<pre><code>snmp:
  addresses:
  - 192.168.200.14/24@mgmt
  - udp6:[::1]:161
  rocommunity: public
</code></pre>
server01
<pre><code>snmp:
  addresses:
  - False/24@mgmt
  - udp6:[::1]:161
  rocommunity: public
</code></pre>
server02
<pre><code>snmp:
  addresses:
  - False/24@mgmt
  - udp6:[::1]:161
  rocommunity: public
</code></pre>
server04
<pre><code>snmp:
  addresses:
  - False/24@mgmt
  - udp6:[::1]:161
  rocommunity: public
</code></pre>
server05
<pre><code>snmp:
  addresses:
  - False/24@mgmt
  - udp6:[::1]:161
  rocommunity: public
</code></pre>
spine01
<pre><code>snmp:
  addresses:
  - 192.168.200.21/24@mgmt
  - udp6:[::1]:161
  rocommunity: public
</code></pre>
spine02
<pre><code>snmp:
  addresses:
  - 192.168.200.22/24@mgmt
  - udp6:[::1]:161
  rocommunity: public
</code></pre>
spine03
<pre><code>snmp:
  addresses:
  - 192.168.200.23/24@mgmt
  - udp6:[::1]:161
  rocommunity: public
</code></pre>
spine04
<pre><code>snmp:
  addresses:
  - 192.168.200.24/24@mgmt
  - udp6:[::1]:161
  rocommunity: public
</code></pre>
</details>
<details><summary markdown="span">evpn_mh</summary>
border01
<pre><code>snmp:
  addresses:
  - 192.168.200.63/24@mgmt
  - udp6:[::1]:161
  rocommunity: public
</code></pre>
border02
<pre><code>snmp:
  addresses:
  - 192.168.200.64/24@mgmt
  - udp6:[::1]:161
  rocommunity: public
</code></pre>
fw1
<pre><code>snmp:
  addresses:
  - False/24@mgmt
  - udp6:[::1]:161
  rocommunity: public
</code></pre>
leaf01
<pre><code>snmp:
  addresses:
  - 192.168.200.11/24@mgmt
  - udp6:[::1]:161
  rocommunity: public
</code></pre>
leaf02
<pre><code>snmp:
  addresses:
  - 192.168.200.12/24@mgmt
  - udp6:[::1]:161
  rocommunity: public
</code></pre>
leaf03
<pre><code>snmp:
  addresses:
  - 192.168.200.13/24@mgmt
  - udp6:[::1]:161
  rocommunity: public
</code></pre>
leaf04
<pre><code>snmp:
  addresses:
  - 192.168.200.14/24@mgmt
  - udp6:[::1]:161
  rocommunity: public
</code></pre>
server01
<pre><code>snmp:
  addresses:
  - False/24@mgmt
  - udp6:[::1]:161
  rocommunity: public
</code></pre>
server02
<pre><code>snmp:
  addresses:
  - False/24@mgmt
  - udp6:[::1]:161
  rocommunity: public
</code></pre>
server04
<pre><code>snmp:
  addresses:
  - False/24@mgmt
  - udp6:[::1]:161
  rocommunity: public
</code></pre>
server05
<pre><code>snmp:
  addresses:
  - False/24@mgmt
  - udp6:[::1]:161
  rocommunity: public
</code></pre>
spine01
<pre><code>snmp:
  addresses:
  - 192.168.200.21/24@mgmt
  - udp6:[::1]:161
  rocommunity: public
</code></pre>
spine02
<pre><code>snmp:
  addresses:
  - 192.168.200.22/24@mgmt
  - udp6:[::1]:161
  rocommunity: public
</code></pre>
spine03
<pre><code>snmp:
  addresses:
  - 192.168.200.23/24@mgmt
  - udp6:[::1]:161
  rocommunity: public
</code></pre>
spine04
<pre><code>snmp:
  addresses:
  - 192.168.200.24/24@mgmt
  - udp6:[::1]:161
  rocommunity: public
</code></pre>
</details>
<details><summary markdown="span">evpn_symmetric</summary>
border01
<pre><code>snmp:
  addresses:
  - 192.168.200.63/24@mgmt
  - udp6:[::1]:161
  rocommunity: public
</code></pre>
border02
<pre><code>snmp:
  addresses:
  - 192.168.200.64/24@mgmt
  - udp6:[::1]:161
  rocommunity: public
</code></pre>
fw1
<pre><code>snmp:
  addresses:
  - False/24@mgmt
  - udp6:[::1]:161
  rocommunity: public
</code></pre>
leaf01
<pre><code>snmp:
  addresses:
  - 192.168.200.11/24@mgmt
  - udp6:[::1]:161
  rocommunity: public
</code></pre>
leaf02
<pre><code>snmp:
  addresses:
  - 192.168.200.12/24@mgmt
  - udp6:[::1]:161
  rocommunity: public
</code></pre>
leaf03
<pre><code>snmp:
  addresses:
  - 192.168.200.13/24@mgmt
  - udp6:[::1]:161
  rocommunity: public
</code></pre>
leaf04
<pre><code>snmp:
  addresses:
  - 192.168.200.14/24@mgmt
  - udp6:[::1]:161
  rocommunity: public
</code></pre>
server01
<pre><code>snmp:
  addresses:
  - False/24@mgmt
  - udp6:[::1]:161
  rocommunity: public
</code></pre>
server02
<pre><code>snmp:
  addresses:
  - False/24@mgmt
  - udp6:[::1]:161
  rocommunity: public
</code></pre>
server04
<pre><code>snmp:
  addresses:
  - False/24@mgmt
  - udp6:[::1]:161
  rocommunity: public
</code></pre>
server05
<pre><code>snmp:
  addresses:
  - False/24@mgmt
  - udp6:[::1]:161
  rocommunity: public
</code></pre>
spine01
<pre><code>snmp:
  addresses:
  - 192.168.200.21/24@mgmt
  - udp6:[::1]:161
  rocommunity: public
</code></pre>
spine02
<pre><code>snmp:
  addresses:
  - 192.168.200.22/24@mgmt
  - udp6:[::1]:161
  rocommunity: public
</code></pre>
spine03
<pre><code>snmp:
  addresses:
  - 192.168.200.23/24@mgmt
  - udp6:[::1]:161
  rocommunity: public
</code></pre>
spine04
<pre><code>snmp:
  addresses:
  - 192.168.200.24/24@mgmt
  - udp6:[::1]:161
  rocommunity: public
</code></pre>
</details>
